" Requires git and https://github.com/junegunn/vim-plug
" Run :PlugInstall on set up

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'nanotech/jellybeans.vim'
Plug 'sbdchd/neoformat'
" Plug 'mattn/emmet-vim'
" Plug '~/.fzf'
" Plug 'lepture/vim-jinja'
" Plug 'will133/vim-dirdiff'

" Common commands:
" PlugInstall [name] - install plugins
" PlugUpdate [name] - update plugin
" PlugClean - remove unused plugins
" PlugUpgrade - upgrade plug

call plug#end()

" most default settings are loaded by vim-sensible above

" custom colours
colorscheme jellybeans

" ignore case when searching
set ignorecase

" allow highlighting search words
set hlsearch

" tab size is 2 spaces
set ts=2

" indent is 2 spaces
set shiftwidth=2

" tab becomes spaces
set expandtab

" show line numbers
set number

" allow a 7-line verticle buffer for the cursor
set so=7

" turn backup off
set nobackup

" set listchars=space:·,tab:»·,trail:¬
" set syntax=whitespace
set list
